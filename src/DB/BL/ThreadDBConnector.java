package DB.BL;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class ThreadDBConnector extends Thread {

	private JLabel lblDBConnectionStatus = null;

	private DBManager manager = null;

	public ThreadDBConnector(DBManager dbManager, JLabel lblStatus) {
		manager = dbManager;
		lblDBConnectionStatus = lblStatus;
	}

	public void run() {
		manager.closeConnection();

		while (!manager.connectToDB(lblDBConnectionStatus) && ConstantsClassDB.boolIsDBManuallyConnected) {
			try {
				System.out.println("Reconneting");
				Thread.sleep(15000); // To delay reconnection request to DB
										// server by 15 seconds in order to
										// avoid
										// network traffic
			} catch (InterruptedException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
			}

		}

		ConstantsClassDB.boolIsThreadRunning = false;
	}
}
