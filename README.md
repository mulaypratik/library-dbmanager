# README #

This is my DBManager repository in Java that contains very useful methods that can be used to handle all types of database
operations (like mainly CRUD operations, DDL, DML and DCL commands). 

### What is this repository for? ###

This contribution will free Java developers to carry the pain of database management, opeing and closing connections, transaction
management, etc. and will leverage them to focus on 
their bussiness logic instead of wasting time on ground level database tasks.

Version: 1.1

### How do I get set up? ###

Open this repository with eclipse.
Run scripts provied in the respository in your MS SQL environment.

### Who do I talk to? ###

Contact me on my email id - mulaypratik30@gmail.com